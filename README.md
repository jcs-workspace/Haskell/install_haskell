# install_haskell

安裝過程, 請看 [Haskell - Installation and Getting Started on Windows](https://www.youtube.com/watch?v=gLr2u6CjSsM&ab_channel=AtoZProgrammingTutorials).

```
choco install haskell-dev
```

安裝結果:

```
Installed:
 - cabal v3.10.1.1
 - chocolatey-compatibility.extension v1.0.0
 - chocolatey-core.extension v1.4.0
 - ghc v9.6.2
 - haskell-dev v0.0.1
 - msys2 v20230718.0.0
```
