@echo off
:: ========================================================================
:: $File: build.bat $
:: $Date: 2023-08-05 23:35:02 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

cd ../

ghc hello.hs
