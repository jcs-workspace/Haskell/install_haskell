-- ========================================================================
-- $File: hello.hs $
-- $Date: 2023-08-05 23:33:01 $
-- $Revision: $
-- $Creator: Jen-Chieh Shen $
-- $Notice: See LICENSE.txt for modification and distribution information
--                   Copyright © 2023 by Shen, Jen-Chieh $
-- ========================================================================

main = do
  print "My first Haskell program"
  name <- getLine
  print ("Hello, " ++ name)
